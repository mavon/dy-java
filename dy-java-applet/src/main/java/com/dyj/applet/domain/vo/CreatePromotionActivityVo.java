package com.dyj.applet.domain.vo;

/**
 * 创建营销活动返回值
 */
public class CreatePromotionActivityVo {

    /**
     * <p>抖音开放平台小程序营销活动ID</p>
     */
    private String activity_id;

    public String getActivity_id() {
        return activity_id;
    }

    public CreatePromotionActivityVo setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }
}
