package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CategoryInfo;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 13:52
 **/
public class GoodsCategoryVo extends DyResult<BaseVo> {

    private List<CategoryInfo> category_list;

    public List<CategoryInfo> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<CategoryInfo> category_list) {
        this.category_list = category_list;
    }
}
