package com.dyj.applet.domain;

/**
 * 创建营销活动-直播间发放
 */
public class PromotionActivityLiveActivity {

    /**
     * <p>创建授权手机号玩法时，线索类小程序 需要关联合法的线索组件。有利于直播间场景下获得流量精准分发，并支持获取完整的线索数据统计。 选填
     */
    private String clue_component_id;
    /**
     * <p>指定用户领券玩法，不传表示不指定玩法 枚举值及含义：0：不指定玩法2：授权手机号玩法，用户领券前需要授权手机号，开放平台会通过callback_url字段将密文手机号回传给开发者，开发者进行解密得到用户手机号。3：关注主播玩法，用户领券前需要关注主播后才能领券。授权手机号玩法使用限制：小程序需要具备获取手机号的能力，申请路径：「抖音开放平台- 小程序 - 能力 - 页面及信息 - 用户能力 - 获取手机号」。 选填
     */
    private Integer coupon_task_type;
    /**
     * <p>发放须知（长度20以内）优惠券发放时候的描述文案，仅对主播展示，用户不可见，比如可向主播传递当前优惠券的稀有程度或口播建议 选填
     */
    private String notice_text;


    public String getClue_component_id() {
        return clue_component_id;
    }

    public PromotionActivityLiveActivity setClue_component_id(String clue_component_id) {
        this.clue_component_id = clue_component_id;
        return this;
    }

    public Integer getCoupon_task_type() {
        return coupon_task_type;
    }

    public PromotionActivityLiveActivity setCoupon_task_type(Integer coupon_task_type) {
        this.coupon_task_type = coupon_task_type;
        return this;
    }



    public String getNotice_text() {
        return notice_text;
    }

    public PromotionActivityLiveActivity setNotice_text(String notice_text) {
        this.notice_text = notice_text;
        return this;
    }

    public static CreatePromotionActivityLiveActivityBuilder builder(){
        return new CreatePromotionActivityLiveActivityBuilder();
    }

    public static final class CreatePromotionActivityLiveActivityBuilder {
        private String clue_component_id;
        private Integer coupon_task_type;
        private String notice_text;

        private CreatePromotionActivityLiveActivityBuilder() {
        }

        public static CreatePromotionActivityLiveActivityBuilder aCreatePromotionActivityLiveActivity() {
            return new CreatePromotionActivityLiveActivityBuilder();
        }

        public CreatePromotionActivityLiveActivityBuilder clueComponentId(String clueComponentId) {
            this.clue_component_id = clueComponentId;
            return this;
        }

        public CreatePromotionActivityLiveActivityBuilder couponTaskType(Integer couponTaskType) {
            this.coupon_task_type = couponTaskType;
            return this;
        }

        public CreatePromotionActivityLiveActivityBuilder noticeText(String noticeText) {
            this.notice_text = noticeText;
            return this;
        }

        public PromotionActivityLiveActivity build() {
            PromotionActivityLiveActivity promotionActivityLiveActivity = new PromotionActivityLiveActivity();
            promotionActivityLiveActivity.setClue_component_id(clue_component_id);
            promotionActivityLiveActivity.setCoupon_task_type(coupon_task_type);
            promotionActivityLiveActivity.setNotice_text(notice_text);
            return promotionActivityLiveActivity;
        }
    }
}
