package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-04-28 10:56
 **/
public class ShopMemberLeaveQuery extends BaseQuery {

    private String app_id;

    private String open_id;

    private Long shop_id;


    public static ShopMemberLeaveQueryBuilder builder() {
        return new ShopMemberLeaveQueryBuilder();
    }

    public static class ShopMemberLeaveQueryBuilder {
        private String appId;
        private String openId;
        private Long shopId;
        private Integer tenantId;
        private String clientKey;

        public ShopMemberLeaveQueryBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }

        public ShopMemberLeaveQueryBuilder openId(String openId) {
            this.openId = openId;
            return this;
        }

        public ShopMemberLeaveQueryBuilder shopId(Long shopId) {
            this.shopId = shopId;
            return this;
        }

        public ShopMemberLeaveQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ShopMemberLeaveQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ShopMemberLeaveQuery build() {
            ShopMemberLeaveQuery shopMemberLeaveQuery = new ShopMemberLeaveQuery();
            shopMemberLeaveQuery.setApp_id(appId);
            shopMemberLeaveQuery.setOpen_id(openId);
            shopMemberLeaveQuery.setShop_id(shopId);
            shopMemberLeaveQuery.setTenantId(tenantId);
            shopMemberLeaveQuery.setClientKey(clientKey);
            return shopMemberLeaveQuery;
        }
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }

    public Long getShop_id() {
        return shop_id;
    }

    public void setShop_id(Long shop_id) {
        this.shop_id = shop_id;
    }
}

