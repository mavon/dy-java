package com.dyj.applet.handler;

import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Var;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DyProductResult;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 16:27
 **/
public class LifeServiceHandler extends AbstractAppletHandler {
    public LifeServiceHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 商铺同步
     *
     * @param query 入参
     * @return DyResult<SupplierSyncVo>
     */
    public DyResult<SupplierSyncVo> supplierSync(SupplierSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().supplierSync(query);
    }

    /**
     * 查询店铺
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierVo>
     */
    public DyResult<SupplierVo> querySupplier(String supplierExtId) {
        return getLifeServicesClient().querySupplier(baseQuery(), supplierExtId);
    }

    /**
     * 获取抖音POI ID
     *
     * @param amapId 高德POI ID
     * @return DyResult<PoiIdVo>
     */
    public DyResult<PoiIdVo> queryPoiId(String amapId) {
        return getLifeServicesClient().queryPoiId(baseQuery(), amapId);
    }


    /**
     * 店铺匹配任务结果查询
     *
     * @param supplierTaskIds 店铺任务ID
     * @return DyResult<SupplierTaskResultVo>
     */
    public DyResult<SupplierTaskResultVo> querySupplierTaskResult(String supplierTaskIds) {
        return getLifeServicesClient().querySupplierTaskResult(baseQuery(), supplierTaskIds);
    }

    /**
     * 店铺匹配任务状态查询
     *
     * @param supplierExtId 店铺ID
     * @return DyResult<SupplierTaskStatusVo>
     */
    public DyResult<SupplierTaskStatusVo> querySupplierMatchStatus(String supplierExtId) {
        return getLifeServicesClient().querySupplierMatchStatus(baseQuery(), supplierExtId);
    }

    /**
     * 提交门店匹配任务
     *
     * @param query 入参
     * @return DyResult<SupplierSubmitTaskVo>
     */
    public DyResult<SupplierSubmitTaskVo> submitSupplierMatchTask(SupplierSubmitTaskQuery query) {
        baseQuery(query);
        return getLifeServicesClient().submitSupplierMatchTask(query);
    }


    /**
     * 查询全部店铺信息接口(天级别请求5次)
     *
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> queryAllSupplier() {
        return getLifeServicesClient().queryAllSupplier(baseQuery());
    }

    /**
     * 查询店铺全部信息任务返回内容
     *
     * @param taskId 任务ID
     * @return DyResult<SupplierTaskVo>
     */
    public DyResult<SupplierTaskVo> querySupplierCallback(String taskId) {
        return getLifeServicesClient().querySupplierCallback(baseQuery(), taskId);
    }

    /**
     * （老版本）SKU同步
     *
     * @param query 入参
     * @return DyResult<BaseVo>
     */
    public DyResult<BaseVo> skuSync(SkuSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().skuSync(query);
    }

    /**
     * （老版本）sku拉取(该接口由接入方实现)
     *
     * @param spuExtId  接入方SPU ID 列表
     * @param startDate 拉取价格时间区间[start_date, end_date)
     * @param endDate   拉取价格时间区间[start_date, end_date)
     * @return DyResult<SkuHotelPullVo>
     */
    public DyResult<SkuHotelPullVo> skuHotelPull(List<String> spuExtId, String startDate, String endDate) {
        return getLifeServicesClient().skuHotelPull(baseQuery(), spuExtId, startDate, endDate);
    }

    /**
     * （老版本）多门店SPU同步
     *
     * @param query 入参
     * @return DyResult<SpuSyncVo>
     */
    public DyResult<SpuSyncVo> spuSync(SpuSyncQuery query) {
        baseQuery(query);
        return getLifeServicesClient().spuSync(query);
    }

    /**
     * （老版本）多门店SPU状态同步
     *
     * @param spuExtIdList 接入方商品ID列表
     * @param status       SPU状态， 1 - 在线; 2 - 下线
     * @return DyResult<SpuStatusVo>
     */
    public DyResult<SpuStatusVo> spuStatusSync(List<String> spuExtIdList, Integer status) {
        return getLifeServicesClient().spuStatusSync(baseQuery(), spuExtIdList, status);
    }

    /**
     * （老版本）多门店SPU库存同步
     *
     * @param spuExtId 接入方商品ID
     * @param stock    库存
     * @return DyResult<SpuStockVo>
     */
    public DyResult<SpuStockVo> spuStockSync(String spuExtId, Long stock) {
        return getLifeServicesClient().spuStockSync(SpuStockQuery.builder()
                .spuExtId(spuExtId)
                .stock(stock)
                .tenantId(agentConfiguration.getTenantId())
                .clientKey(agentConfiguration.getClientKey())
                .build());
    }

    /**
     * （老版本）多门店SPU信息查询
     *
     * @param spuExtId                   第三方SPU ID
     * @param needSpuDraft               是否需要商品草稿数据(带有商品的审核状态，只展示最近30天的数据，并且最多最近提交的20次内)
     * @param spuDraftCount              需要商品草稿数据的数目(0-20)，超过这个范围默认返回20个
     * @param supplierIdsForFilterReason 供应商id列表，需要商品在某供应商下的过滤状态
     * @return DyResult<SpuVo>
     */
    public DyResult<SpuVo> spuQuery(String spuExtId, Boolean needSpuDraft, Integer spuDraftCount, List<String> supplierIdsForFilterReason) {
        return getLifeServicesClient().spuQuery(baseQuery(), spuExtId, needSpuDraft, spuDraftCount, supplierIdsForFilterReason);
    }

    /**
     * 创建/修改团购商品
     *
     * @param query 入参
     * @return DyProductResult<SaveGoodsProductVo>
     */
    public DyProductResult<SaveGoodsProductVo> saveGoodsProduct(SaveGoodsProductQuery query) {
        baseQuery(query);
        return getLifeServicesClient().saveGoodsProduct(query);
    }

    /**
     * 免审修改商品
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> freeAuditGoodsProduct(FreeAuditGoodsProductQuery query) {
        baseQuery(query);
        return getLifeServicesClient().freeAuditGoodsProduct(query);
    }

    /**
     * 上下架商品
     *
     * @param productId 商品ID
     * @param outId     商品外部ID
     * @param opType    操作类型  1-上线 2-下线
     * @return DyProductResult<String>
     */
    public DyProductResult<String> operateGoodsProduct(String productId, String outId, Integer opType) {
        return getLifeServicesClient().operateGoodsProduct(OperateGoodsProductQuery.build()
                .productId(productId)
                .outId(outId)
                .opType(opType)
                .tenantId(agentConfiguration.getTenantId())
                .clientKey(agentConfiguration.getClientKey())
                .build());
    }

    /**
     * 同步库存
     *
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> syncGoodsStock(SyncGoodsStockQuery query) {
        baseQuery(query);
        return getLifeServicesClient().syncGoodsStock(query);
    }

    /**
     * 查询商品模板
     *
     * @param categoryId  行业类目；详细见； 商品类目表
     * @param productType 商品类型 1 : 团购套餐 3 : 预售券 4 : 日历房 5 : 门票 7 : 旅行跟拍 8 : 一日游 11 : 代金券
     * @return DyProductResult<GoodsTemplateVo>
     */
    public DyProductResult<GoodsTemplateVo> getGoodsTemplate(String categoryId, Integer productType) {
        return getLifeServicesClient().getGoodsTemplate(baseQuery(), categoryId, productType);
    }

    /**
     * 查询商品草稿数据
     *
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductDraftVo>
     */
    public DyProductResult<GoodsProductDraftVo> getGoodsProductDraft(String productIds, String outIds) {
        return getLifeServicesClient().getGoodsProductDraft(baseQuery(), productIds, outIds);
    }

    /**
     * 查询商品线上数据
     *
     * @param productIds 商品ID列表（逗号分隔）
     * @param outIds     外部商品ID列表（逗号分隔）
     * @return DyProductResult<GoodsProductOnlineVo>
     */
    public DyProductResult<GoodsProductOnlineVo> getGoodsProductOnline(String productIds, String outIds) {
        return getLifeServicesClient().getGoodsProductOnline(baseQuery(), productIds, outIds);
    }

    /**
     * 查询商品线上数据列表
     *
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count  分页数量，不传默认为5
     * @param status 过滤在线状态 1-在线 2-下线 3-封禁
     * @return DyProductResult
     */
    public DyProductResult<GoodsProductOnlineVo> queryGoodsProductOnlineList(String cursor, Integer count, Integer status) {
        return getLifeServicesClient().queryGoodsProductOnlineList(baseQuery(), cursor, count, status);
    }

    /**
     * 查询商品草稿数据列表
     * @param cursor 第一页不传，之后用前一次返回的next_cursor传入进行翻页
     * @param count 分页数量，不传默认为5
     * @param status 过滤草稿状态，10-审核中 12-审核失败 1-审核通过
     * @return DyProductResult<GoodsProductDraftVo>
     */
    public DyProductResult<GoodsProductDraftVo> queryGoodsProductDraftList(String cursor, Integer count, Integer status){
        return getLifeServicesClient().queryGoodsProductDraftList(baseQuery(), cursor, count, status);
    }

    /**
     * 创建/更新多SKU商品的SKU列表
     * @param query 入参
     * @return DyProductResult<String>
     */
    public DyProductResult<String> batchSaveGoodsSku(BatchSaveGoodsSkuQuery query){
        baseQuery(query);
        return getLifeServicesClient().batchSaveGoodsSku(query);
    }

    /**
     * 查询商品品类
     * @param categoryId 行业类目ID，返回当前id下的直系子类目信息；传0或者不传，均返回所有一级行业类目
     * @param accountId 服务商的入驻商户ID/代运营的商户ID，不传时默认为服务商身份
     * @return GoodsCategoryVo
     */
    public GoodsCategoryVo getGoodsCategory(String categoryId, String accountId){
        return getLifeServicesClient().getGoodsCategory(baseQuery(), categoryId, accountId);
    }
}
